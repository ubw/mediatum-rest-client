# mediatum-rest-client

> Convenience wrapper around the RESTful [mediaTUM](https://github.com/mediatum/mediatum) API


## Install

```
$ npm install @ubw/mediatum-rest-client
```


## Usage

```js
const ubwMediatumRestClient = require('@ubw/mediatum-rest-client');

ubwMediatumRestClient.getAllChildren(1433089, {limit: 1}).then(console.log);
// =>
/*
{
	data: [
		{
			type: '1433089',
			id: 1335274,
			attributes: {
				'author.firstname': 'Thomas',
				type: 'Dissertation',
				// ...
			},
			links: {
				self: {
					href: 'https://mediatum.ub.tum.de/?id=...'
				}
			}
		}
	],
	meta: {
		username: 'guest',
		status: 'ok',
		method: 'GET',
		nodelist_limit: '1',
		nodelist_start: 0,
		// ...
	}
}
 */
```


## API


### .getAllChildren(id, [options])

Returns a `Promise`.


#### id

Type: `Number`

The [`id` of the mediaTUM node](https://mediatum.ub.tum.de/services/export/) to query.


#### options

Type: `Object`

See [mediaTUM query parameters](https://mediatum.ub.tum.de/services/export) for additional options.


##### format

Type: `String`<br>
Default: `'json'`


##### acceptcached

Type: `Number`<br>
Default: `180`


##### mask

Type: `String`<br>
Default: `'none'`


##### attrspec

Type: `String`<br>
Default: `'all'`


##### baseUrl

Type: `String`<br>
Default: `'https://mediatum.ub.tum.de'`


##### queryUrl

Type: `Function`<br>
Default: `` (baseUrl, id, endpoint) => `${baseUrl}/services/export/node/${id}/${endpoint}` ``


##### selfHref

Type: `Function`<br>
Default: `` (node, baseUrl) => `${baseUrl}/?id=${node.id}` ``


##### schema

Type: `Object`<br>
Default: `undefined`

Expected `attributes` schema.

```js
const schema = {
	author: String,      // convert attribute to string
	title: undefined,    // include attribute, but do not mind the type
	isAwesome: Boolean,  // convert attribute to boolean
	year: 'year',        // convert attribute to integer
	pages: 'int',        // convert attribute to integer
	created: Date,       // convert attribute to date
};

getAllChildren(42, {schema})
// => nodes in nodelist will only have attributes mentioned above
/*
{
	data: [
		{
			type: '21',
			id: 42,
			attributes: {
				author: 'Douglas Adams',
				title: 'The Hitchhiker’s Guide to the Galaxy',
				isAwesome: true,
				year: 1979,
				pages: 158,
				created: Date(...)
			},
			links: {
				self: {
					href: 'https://mediatum.ub.tum.de/?id=42'
				}
			}
		}
	],
	meta: {
		// ...
	}
}
 */
```


## Related

* [mediaTUM - Medienserver](https://mediatum.github.io)
* [mediaTUM Core](https://github.com/mediatum/mediatum)
* [@ubw/mediatum-query-builder](https://gitlab.com/ubw/mediatum-query-builder) – Convenience query builder for the RESTful mediaTUM API


## License

MIT © [Michael Mayer](http://schnittstabil.de)
