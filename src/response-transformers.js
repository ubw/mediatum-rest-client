const transformAttributeValue = (value, type) => {
	// Workaround missing input validation of mediaTUM's web user interface
	if (typeof value === 'string') {
		value = value.trim();
		if (value === '') {
			return null;
		}
	}

	if (type === undefined || value === null) {
		return value;
	}

	if (type === String) {
		const v = String(value).trim();
		return v === '' ? null : v;
	}

	if (type === 'year' || type === 'int') {
		const v = parseInt(value, 10);
		return isNaN(v) ? value : v;
	}

	if (type === Date) {
		const v = new Date(value);
		return isNaN(v) ? value : v;
	}

	if (type === Boolean) {
		const v = String(value).trim().toLowerCase();
		if (['true', 'on', 'yes'].includes(v)) {
			return true;
		}

		if (['false', 'off', 'no'].includes(v)) {
			return false;
		}

		return Boolean(value);
	}

	return value;
};

const transformAttributes = (attributes, schema) => {
	if (!(schema instanceof Object)) {
		return attributes;
	}

	const sanitized = {};

	for (const key of Object.keys(schema).sort()) {
		const value = attributes[key];
		if (value !== undefined) {
			sanitized[key] = transformAttributeValue(value, schema[key]);
		}
	}

	return sanitized;
};

const transformNode = (type, node, {baseUrl, selfHref, schema}) => ({
	id: node.id,
	type: String(type),
	attributes: transformAttributes(node.attributes, schema),
	links: {
		self: {
			href: selfHref(node, baseUrl)
		}
	}
});

const transformNodelist = (type, nodelist, opts) => {
	nodelist = nodelist || [];
	const sanitized = [];

	for (let i = 0; i < nodelist.length; i++) {
		const nodes = nodelist[i] || [];
		for (let j = 0; j < nodes.length; j++) {
			sanitized.push(transformNode(type, nodes[j], opts));
		}
	}

	return sanitized;
};

const transformResponse = (type, opts) => response => {
	const {nodelist, ...meta} = response.data;
	return {
		data: transformNodelist(type, nodelist, opts),
		meta
	};
};

exports.transformNode = transformNode;
exports.transformNodelist = transformNodelist;
exports.transformResponse = transformResponse;
