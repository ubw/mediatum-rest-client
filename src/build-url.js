const encodeParam = (key, value) => {
	key = encodeURIComponent(key);

	if (key === '' || value === null) {
		return key;
	}

	return key + '=' + encodeURIComponent(value);
};

const buildQueryString = params => {
	if (!(params instanceof Object)) {
		return '';
	}

	const parts = [];

	for (const key of Object.keys(params).sort()) {
		const value = params[key];

		if (value === undefined) {
			continue;
		}

		parts.push(encodeParam(key, value));
	}

	return parts.filter(part => part.length > 0).join('&');
};

module.exports = (url, params) => {
	const query = buildQueryString(params);

	if (query.length === 0) {
		return url;
	}

	const glue = url.includes('?') ? '&' : '?';

	return url + glue + query;
};
