const buildUrl = require('./build-url');
const {transformResponse} = require('./response-transformers');

const defaults = {
	format: 'json',
	acceptcached: 180,
	mask: 'none',
	attrspec: 'all',
	schema: undefined,
	baseUrl: 'https://mediatum.ub.tum.de',
	queryUrl: (baseUrl, id, endpoint) => `${baseUrl}/services/export/node/${id}/${endpoint}`,
	selfHref: (node, baseUrl) => `${baseUrl}/?id=${node.id}`
};

const tryToDecodeJson = response => response.json()
	.then(data => {
		response.data = data;
		return response;
	})
	.catch(_ => response);

const createErrorFromResponse = (msg, response, status) => {
	const error = new Error(msg);
	error.response = response;
	error.status = status;

	return error;
};

const checkStatus = response => {
	let {data, status} = response;

	if (data && 'html_response_code' in data) {
		// WTF!? - Goddamn, why mediaTUM? WHY?
		status = parseInt(data.html_response_code, 10);
	}

	if (status >= 200 && status < 300) {
		return response;
	}

	throw createErrorFromResponse(response.statusText, response, status);
};

const getAllChildren = (id, opts) => {
	const {
		baseUrl,
		selfHref,
		queryUrl,
		schema,
		...queryParams
	} = Object.assign({}, defaults, opts);

	const url = buildUrl(queryUrl(baseUrl, id, 'allchildren'), queryParams);

	// NB: async/await polyfill > 10 kB
	return fetch(url)
		.then(tryToDecodeJson)
		.then(checkStatus)
		.then(transformResponse(id, {baseUrl, selfHref, schema}));
};

exports.defaults = defaults;
exports.getAllChildren = getAllChildren;
