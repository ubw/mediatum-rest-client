import test from 'ava';
import {defaults} from '../src';
import {transformNode, transformNodelist, transformResponse} from '../src/response-transformers';

class EmptyMock {
	toString() {
		return '  ';
	}
}

const emptyMock = new EmptyMock();

const type = 9990;

const response = {
	data: {
		username: 'guest',
		status: 'ok',
		nodelist: [
			[
				{
					attributes: {
						author: 'Douglas Adams',
						title: 'The Hitchhiker’s Guide to the Galaxy',
						publisher: 'Pan Books',
						empty: '  ',
						emptyObject: emptyMock,
						isAwesome: 'yes',
						malformedDate: '1979-00-00T00:00:00',
						malformedInt: 'forty-two',
						moronicYear: '1979-00-00T00:00:00',
						nullable: '',
						hideMe: 'please!',
						pants: 'off',
						rocks: '1',
						type: 'book',
						undefinedable: undefined,
						updatetime: '1980-01-01T12:00:00',
						year: '1979'
					},
					id: 42
				}
			]
		],
		method: 'GET'
	}
};

const expectedResponse = {
	data: [
		{
			type: String(type),
			id: 42,
			attributes: {
				author: 'Douglas Adams',
				title: 'The Hitchhiker’s Guide to the Galaxy',
				publisher: 'Pan Books',
				empty: '  ',
				emptyObject: emptyMock,
				isAwesome: 'yes',
				malformedDate: '1979-00-00T00:00:00',
				malformedInt: 'forty-two',
				moronicYear: '1979-00-00T00:00:00',
				nullable: '',
				hideMe: 'please!',
				pants: 'off',
				rocks: '1',
				type: 'book',
				undefinedable: undefined,
				updatetime: '1980-01-01T12:00:00',
				year: '1979'
			},
			links: {
				self: {
					href: 'https://mediatum.ub.tum.de/?id=42'
				}
			}
		}
	],
	meta: {
		username: 'guest',
		status: 'ok',
		method: 'GET'
	}
};

const schema = {
	author: String,
	title: undefined,
	publisher: 'unknown',
	deprecated: undefined,
	empty: undefined,
	emptyObject: String,
	isAwesome: Boolean,
	moronicYear: 'year',
	malformedDate: Date,
	malformedInt: 'int',
	nullable: undefined,
	pants: Boolean,
	rocks: Boolean,
	type: String,
	undefinedable: undefined,
	updatetime: Date,
	year: 'year'
};

const expectedSchemedResponse = {
	data: [
		{
			id: 42,
			type: String(type),
			attributes: {
				author: 'Douglas Adams',
				title: 'The Hitchhiker’s Guide to the Galaxy',
				publisher: 'Pan Books',
				empty: null,
				emptyObject: null,
				isAwesome: true,
				malformedDate: '1979-00-00T00:00:00',
				malformedInt: 'forty-two',
				moronicYear: 1979,
				nullable: null,
				pants: false,
				rocks: true,
				type: 'book',
				updatetime: new Date('1980-01-01T12:00:00'),
				year: 1979
			},
			links: {
				self: {
					href: 'https://mediatum.ub.tum.de/?id=42'
				}
			}
		}
	],
	meta: {
		username: 'guest',
		status: 'ok',
		method: 'GET'
	}
};

test('transformNode', t => {
	t.deepEqual(transformNode(type, response.data.nodelist[0][0], defaults), expectedResponse.data[0]);
});

test('transformNode with schema', t => {
	const opts = {
		...defaults,
		schema
	};
	t.deepEqual(transformNode(type, response.data.nodelist[0][0], opts), expectedSchemedResponse.data[0]);
});

test('transformNodelist', t => {
	const guide = response.data.nodelist[0][0];
	const expectedGuide = expectedResponse.data[0];
	t.deepEqual(transformNodelist(type, [[guide]], defaults), [expectedGuide]);
	t.deepEqual(transformNodelist(type, [[guide, guide]], defaults), [expectedGuide, expectedGuide]);
	t.deepEqual(transformNodelist(type, undefined, {}), []);
	t.deepEqual(transformNodelist(type, [], defaults), []);
	t.deepEqual(transformNodelist(type, [undefined], defaults), []);
});

test('transformResponse', t => {
	t.deepEqual((transformResponse(type, defaults)(response)), expectedResponse);
});

test('transformResponse with schema', t => {
	const opts = {
		...defaults,
		schema
	};
	t.deepEqual(transformResponse(type, opts)(response), expectedSchemedResponse);
});
