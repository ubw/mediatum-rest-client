import test from 'ava';
import buildUrl from '../src/build-url';

const url = 'https://example.com';

test('simple', t => {
	t.is(buildUrl(url), url);
	t.is(buildUrl(url, {}), url);

	t.is(buildUrl(url, {
		unicorns: 42,
		leprechaun: 0
	}), url + '?leprechaun=0&unicorns=42');
});

test('do not return undefined values', t => {
	t.is(buildUrl(url, {
		unicorns: undefined
	}), url);
});

test('do not return empty keys', t => {
	t.is(buildUrl(url, {
		'': 42
	}), url);
});

test('glue wisely', t => {
	const urlWithParams = url + '?leprechaun=0';
	t.is(buildUrl(urlWithParams, {
		unicorns: 42
	}), urlWithParams + '&unicorns=42');
});
