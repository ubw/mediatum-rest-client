import crypto from 'crypto';
import test from 'ava';
import fetch from 'node-fetch';
import {getAllChildren} from '../src';

const hochschulbibliographie = 1433087;
const queryUrl404 = baseUrl => baseUrl + '/' + crypto.randomBytes(32).toString('hex');

global.fetch = fetch;

test('smoke', async t => {
	const {data, meta} = await getAllChildren(hochschulbibliographie, {limit: 42});
	t.is(meta.status, 'ok');
	t.is(data.length, 42);
});

test('throw 404 on not found', async t => {
	const error = await t.throws(getAllChildren(0, {queryUrl: queryUrl404}));
	t.true(error instanceof Error);
	t.is(error.status, 404);
});

test('throw not found on node not found', async t => {
	const error = await t.throws(getAllChildren(0));
	t.true(error instanceof Error);
	t.is(error.status, 404);

	const {data} = error.response;
	t.is(data.status, 'fail');
	t.is(data.errormessage, 'node not found');
	t.is(parseInt(data.html_response_code, 10), error.status);
});

test('usage', async t => {
	const {data} = await getAllChildren(1433089, {limit: 1});
	t.is(data.length, 1);

	const node = data[0];
	t.true('author.firstname' in node.attributes);
	t.true('id' in node);
	t.true('type' in node);
	t.true('links' in node);
	t.true('self' in node.links);
	t.true('href' in node.links.self);

	t.is(node.type, '1433089');
});
